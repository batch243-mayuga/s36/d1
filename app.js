// setup dependencies
const express = require('express');
const mongoose = require('mongoose');

// create an application using express function
const app = express();
const port = 3001;

// This allows us to use all the routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute")

app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Allow all the task routes created in "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute)

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.cyvdebm.mongodb.net/B243-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);






app.listen(port, () => console.log(`Server running at port ${port}`));